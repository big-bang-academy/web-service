import { Controller, Get, Logger } from '@nestjs/common';
import { AppService } from './app.service';
import { promises as fsPromises } from 'fs';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private readonly logger: Logger,
  ) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Get('/api/users')
  async getUsers() {
    try {
      const data = await fsPromises.readFile('data.json', 'utf8');
      const users = JSON.parse(data).users;
      return users;
    } catch (error) {
      this.logger.error(error.message);
      throw new Error(error.message);
    }
  }
}
